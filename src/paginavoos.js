import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Modal, Button, ScrollView, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import sheets from "./axios/axios";

// Componente principal da página de voos
const PaginaVoos = () => {
  const navigation = useNavigation(); // Hook de navegação
  const [showModal, setShowModal] = useState(false); // Estado para controlar a exibição do modal
  const [selectedPassagem, setSelectedPassagem] = useState(null); // Estado para armazenar a passagem selecionada

  // Função para voltar à tela anterior
  const handleBack = () => {
    navigation.goBack();
  };

  // Função para exibir os detalhes de uma passagem
  const handleDetalhesPassagem = (passagem) => {
    setSelectedPassagem(passagem);
    setShowModal(true);
  };

  // Função para realizar a reserva de uma passagem
  const handleReserva = async (passagem) => {
    try {
      const reservaData = {
        Cliente_ID: passagem.Cliente_ID,
        Voo_Numero: passagem.Voo_Numero,
        AssentosReservados: passagem.AssentosReservados,
        ValorTotal: passagem.ValorTotal,
        DataReserva: passagem.DataReserva,
        HoraReserva: passagem.HoraReserva
      };

      // Verificação de campos obrigatórios
      for (const key in reservaData) {
        if (!reservaData[key]) {
          Alert.alert("Erro", `O campo ${key} está vazio ou inválido.`);
          return;
        }
      }

      // Envio dos dados da reserva para o servidor
      const response = await sheets.postReserva(reservaData);
      if (response && response.status === 201) {
        Alert.alert("Reserva realizada com sucesso!");
        setShowModal(false);
      }
    } catch (error) {
      console.error("Erro ao reservar passagem: ", error.response ? error.response.data : error.message);
      Alert.alert("Erro ao reservar passagem", error.response ? error.response.data.message : "Por favor, tente novamente.");
    }
  };

  // Lista de passagens para exibição
  const passagens = [
    {
      Cliente_ID: 10,
      Voo_Numero: 2,
      AssentosReservados: 2,
      ValorTotal: 300.00,
      DataReserva: "2024-06-13",
      HoraReserva: "10:00:00"
    },
    {
      Cliente_ID: 10,
      Voo_Numero: 2,
      AssentosReservados: 1,
      ValorTotal: 150.00,
      DataReserva: "2024-06-14",
      HoraReserva: "11:00:00"
    },
    {
      Cliente_ID: 10,
      Voo_Numero: 2,
      AssentosReservados: 3,
      ValorTotal: 450.00,
      DataReserva: "2024-06-15",
      HoraReserva: "12:00:00"
    },
    {
      Cliente_ID: 10,
      Voo_Numero: 2,
      AssentosReservados: 2,
      ValorTotal: 300.00,
      DataReserva: "2024-06-16",
      HoraReserva: "13:00:00"
    }
  ];

  // Componente para exibir cada passagem aérea
  const PassagemAereaBox = ({ passagem, onPress }) => (
    <View style={styles.box}>
      <Text style={styles.label}>Número: {passagem.Voo_Numero}</Text>
      <Text style={styles.label}>Assentos Reservados: {passagem.AssentosReservados}</Text>
      <Text style={styles.label}>Valor Total: {passagem.ValorTotal}</Text>
      <Text style={styles.label}>Data Reserva: {passagem.DataReserva}</Text>
      <Text style={styles.label}>Hora Reserva: {passagem.HoraReserva}</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => onPress(passagem)}>
          <Text style={styles.buttonText}>Reservar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={handleBack}>
          <Text style={styles.text}>Voltar</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.message}>
        Escolha a sua passagem
      </Text>
      <ScrollView style={styles.scrollView}>
        {passagens.map((passagem, index) => (
          <PassagemAereaBox
            key={index}
            passagem={passagem}
            onPress={handleDetalhesPassagem}
          />
        ))}
      </ScrollView>

      <Modal visible={showModal} animationType="slide" transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>Detalhes da Passagem:</Text>
            {selectedPassagem && (
              <>
                <Text style={styles.label}>Número: {selectedPassagem.Voo_Numero}</Text>
                <Text style={styles.label}>Assentos Reservados: {selectedPassagem.AssentosReservados}</Text>
                <Text style={styles.label}>Valor Total: {selectedPassagem.ValorTotal}</Text>
                <Text style={styles.label}>Data Reserva: {selectedPassagem.DataReserva}</Text>
                <Text style={styles.label}>Hora Reserva: {selectedPassagem.HoraReserva}</Text>
              </>
            )}
            <View style={styles.buttonContainer}>
              <Button title="Reservar" onPress={() => handleReserva(selectedPassagem)} />
              <Button title="Fechar" onPress={() => setShowModal(false)} />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

// Estilos do componente
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  header: {
    marginBottom: 20,
  },
  text: {
    fontSize: 15,
    color: "#56409e",
  },
  message: {
    fontSize: 20,
    marginBottom: 20,
  },
  scrollView: {
    width: '100%',
  },
  box: {
    backgroundColor: "#dfdfdf",
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    width: '100%',
  },
  label: {
    fontSize: 16,
    marginBottom: 10,
  },
  button: {
    backgroundColor: "#56409e",
    paddingVertical: 9,
    paddingHorizontal: 8,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
    marginTop: 10,
    width: '100%',
  },
  buttonText: {
    fontSize: 15,
    color: "#fff",
  },
  modalBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContainer: {
    width: 300,
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  buttonContainer: {
    marginTop: 10,
  },
});

export default PaginaVoos;
