import axios from "axios";

const api = axios.create({
  baseURL: "http://10.89.234.193:5000/passagem/",
  headers: {
    accept: "application/json",
  },
});

const sheets = {
  postReserva: (reserva) => api.post("/criarReserva", reserva),
  login: (cliente) => api.post(`/loginCliente`, cliente),
  cadastro: (cliente) => api.post(`/cliente`, cliente),
  getClientes: () => api.get(`/listaCliente`),
  //deleteCliente: (id) => api.delete(`/deleteCliente/${id}`), // Corrigi a URL para incluir a variável id
  //updateCliente: (id, cliente) => [AxiosError: Request failed with status code 400]api.put(`/atualizarCliente/${id}`, cliente), // Adicionei o parâmetro cliente para a atualização
}

export default sheets;
