import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  ImageBackground,
  Text,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function HomeScreen() {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.hero}>
        <ImageBackground
          source={{
            uri: "https://img.freepik.com/vetores-premium/viajar-ao-redor-do-mundo_1270-446.jpg?w=740",
          }}
          style={styles.heroImage}
          resizeMode="contain"

        />
      </View>
      <View style={styles.content}>
        <View style={styles.contentHeader}>
          <Text style={styles.title}>
            Planeje sua viagem{"\n"}com{" "}
            <View style={styles.appName}>
              <Text style={styles.appNameText}>o nosso APP</Text>
            </View>
          </Text>
          <Text style={styles.text}>
            Aproveite as suas férias com os melhores pacotes, no melhor site de
            viagens!
          </Text>
        </View>

        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Vamos nessa!</Text>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 28,
    fontWeight: "500",
    color: "#281b52",
    textAlign: "center",
    marginBottom: 12,
    lineHeight: 40,
  },
  text: {
    fontSize: 15,
    lineHeight: 24,
    fontWeight: "400",
    color: "#9992a7",
    textAlign: "center",
  },
  
  hero: {
    margin: 12,
    borderRadius: 25,
    overflow: "hidden", // Para garantir que a borda arredondada seja aplicada
  },
  heroImage: {
    width: "100%",
    height: 400,
    borderRadius: 25,
  },
 
  content: {
    flex: 1,
    justifyContent: "space-between",
    paddingVertical: 24,
    paddingHorizontal: 24,
  },
  contentHeader: {
    paddingHorizontal: 24,
  },
  appName: {
    backgroundColor: "#fff2dd",
    transform: [
      {
        rotate: "-3deg",
      },
    ],
    paddingHorizontal: 6,
  },
  appNameText: {
    fontSize: 28,
    fontWeight: "700",
    color: "#281b52",
  },
  /** Button */
  button: {
    backgroundColor: "#56409e",
    paddingVertical: 12,
    paddingHorizontal: 14,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "500",
    color: "#fff",
  },
});
