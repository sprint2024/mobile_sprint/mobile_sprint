import React, { useState } from "react";
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
} from "react-native";
import sheets from "./axios/axios";

// Componente principal de cadastro
const Cadastro = ({ navigation }) => {
  // Estados para armazenar os valores dos campos do formulário
  const [nome, setNome] = useState("");
  const [senha, setSenha] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [cpf, setCpf] = useState("");
  const [cep, setCep] = useState("");

  // Função para formatar o CPF ao ser digitado
  const handleChange = (text) => {
    const formattedCpf = formatCpf(text);
    setCpf(formattedCpf);
  };

  // Função para tratar o cadastro do usuário
  async function handleCadastrar() {
    try {
      // Verifica se todos os campos estão preenchidos
      if (nome && senha && confirmPassword && telefone && email && cpf && cep) {
        // Verifica se as senhas coincidem
        if (senha !== confirmPassword) {
          Alert.alert("Erro", "As senhas informadas não coincidem");
        } else {
          // Faz a requisição para cadastrar o usuário
          const response = await sheets.cadastro({
            nome,
            senha,
            confirmPassword,
            telefone,
            email,
            cpf,
            cep,
          });
          Alert.alert("Cliente cadastrado com sucesso!");
          navigation.navigate("paginavoos");
        }
      } else {
        Alert.alert("Erro", "Por favor, preencha todos os campos.");
      }
    } catch (error) {
      console.error("Erro ao cadastrar cliente: ", error);
      if (error.response && error.response.data) {
        console.error("Detalhes do erro: ", error.response.data);
        Alert.alert("Erro", error.response.data.error || "Erro desconhecido ao cadastrar cliente.");
      } else {
        Alert.alert("Erro", "Não foi possível cadastrar o cliente. Tente novamente.");
      }
    }
  }

  // Função para formatar o CPF conforme o usuário digita
  function formatCpf(cpf) {
    // cpf = cpf.replace(/\D/g, ""); // Remove caracteres não numéricos
    // if (cpf.length > 3) {
    //   cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    // }
    // if (cpf.length > 6) {
    //   cpf = cpf.replace(/(\d{3})(\d{3})(\d)/, "$1.$2.$3");
    // }
    // if (cpf.length > 9) {
    //   cpf = cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    // }
    return cpf;
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Image
              alt="App Logo"
              resizeMode="contain"
              style={styles.headerImg}
              source={{ uri: "https://assets.withfra.me/SignIn.2.png" }}
            />
            <Text style={styles.title}>
              Crie sua <Text style={{ color: "#56409e" }}>conta</Text>
            </Text>
            <Text style={styles.text}>Comece a embarcar com a gente!</Text>
          </View>

          <View style={styles.form}>
            {/* Campo para nome */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Nome</Text>
              <TextInput
                placeholder="Seu nome"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={nome}
                onChangeText={setNome}
              />
            </View>

            {/* Campo para email */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Email</Text>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                placeholder="email@exemplo.com"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={email}
                onChangeText={setEmail}
              />
            </View>

            {/* Campo para telefone */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Telefone</Text>
              <TextInput
                keyboardType="phone-pad"
                placeholder="(XX) XXXXX-XXXX"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={telefone}
                onChangeText={setTelefone}
              />
            </View>

            {/* Campo para CEP */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>CEP</Text>
              <TextInput
                keyboardType="numeric"
                placeholder="XXXXXXXX"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={cep}
                onChangeText={setCep}
              />
            </View>

            {/* Campo para senha */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Senha</Text>
              <TextInput
                autoCorrect={false}
                placeholder="********"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                secureTextEntry={true}
                value={senha}
                onChangeText={setSenha}
              />
            </View>

            {/* Campo para confirmar senha */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Confirmar Senha</Text>
              <TextInput
                autoCorrect={false}
                placeholder="********"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                secureTextEntry={true}
                value={confirmPassword}
                onChangeText={setConfirmPassword}
              />
            </View>

            {/* Campo para CPF */}
            <View style={styles.input}>
              <Text style={styles.inputLabel}>CPF</Text>
              <TextInput
                keyboardType="numeric"
                placeholder="XXX.XXX.XXX-XX"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={cpf}
                onChangeText={handleChange}
                maxLength={11}
              />
            </View>

            {/* Botão para cadastrar */}
            <View style={styles.formAction}>
              <TouchableOpacity onPress={handleCadastrar}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Cadastrar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

// Estilos do componente
const styles = StyleSheet.create({
  scrollViewContent: {
    flexGrow: 1,
    paddingVertical: 24,
    paddingHorizontal: 0,
  },
  container: {
    paddingVertical: 24,
    paddingHorizontal: 24, // Corrigido para manter o padding horizontal consistente
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  title: {
    fontSize: 28,
    fontWeight: "500",
    color: "#281b52",
    textAlign: "center",
    marginBottom: 12,
    lineHeight: 40,
  },
  text: {
    fontSize: 15,
    lineHeight: 24,
    fontWeight: "400",
    color: "#9992a7",
    textAlign: "center",
  },
  header: {
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 36,
  },
  headerImg: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginBottom: 36,
  },
  form: {
    marginBottom: 24,
    paddingHorizontal: 24,
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  formAction: {
    marginTop: 4,
    marginBottom: 16,
  },
  formLink: {
    fontSize: 16,
    fontWeight: "600",
    color: "#075eec",
    textAlign: "center",
  },
  formFooter: {
    fontSize: 15,
    fontWeight: "600",
    color: "#222",
    textAlign: "center",
    letterSpacing: 0.15,
  },
  input: {
    marginBottom: 16,
  },
  inputLabel: {
    fontSize: 17,
    fontWeight: "600",
    color: "#222",
    marginBottom: 8,
  },
  inputControl: {
    height: 50,
    backgroundColor: "#fff",
    paddingHorizontal: 16,
    borderRadius: 12,
    fontSize: 15,
    fontWeight: "500",
    color: "#222",
    borderWidth: 1,
    borderColor: "#C9D3DB",
    borderStyle: "solid",
  },
  button: {
    backgroundColor: "#56409e",
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 12,
    marginTop: 24,
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "600",
  },
});

export default Cadastro;
