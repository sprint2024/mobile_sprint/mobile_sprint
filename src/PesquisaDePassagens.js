import React, { useState } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useNavigation } from "@react-navigation/native";
import  sheets from "./axios/axios"

export default function PesquisaDePassagens() {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const navigation = useNavigation();
  const [origem, setOrigem] = useState('');
  const [destino, setDestino] = useState('');
  const [ida, setIda] = useState('');
  const [volta, setVolta] = useState('');
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [isSelectingIda, setIsSelectingIda] = useState(true);

  const formatDate = (date) => {
    return date.toISOString().split('T')[0]; // Formata a data para 'YYYY-MM-DD'
  }

  async function handleSearch() {
    if (origem === "" || destino === "" || ida === "" || volta === "") {
      Alert.alert("Erro", "Por favor, preencha todos os campos.");
      return;
    }

    try {
      const voos = await buscarVoos(origem, destino, ida, volta);
      if (voos.length === 0) {
        Alert.alert("Nenhum voo encontrado", "Não foram encontrados voos com as informações fornecidas.");
      } else {
        navigation.navigate('paginavoos', { voos });
      }
    } catch (error) {
      console.error("Erro ao buscar voos:", error);
      Alert.alert("Erro", "Erro ao buscar voos. Por favor, tente novamente.");
    }
  };

  const handleSelectDate = (event, date) => {
    setShowDatePicker(false);
    if (date) {
      if (isSelectingIda) {
        setIda(formatDate(date));
      } else {
        setVolta(formatDate(date));
      }
    }
  };

  const openDatePicker = (selectingIda) => {
    setIsSelectingIda(selectingIda);
    setShowDatePicker(true);
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <KeyboardAwareScrollView>
          <View style={styles.header}>
            <Text style={styles.title}>Pesquisa De Passagens</Text>
          </View>

          <View style={styles.form}>
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Origem</Text>
              <TextInput
                placeholder="Origem"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={origem}
                onChangeText={setOrigem}
              />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Destino</Text>
              <TextInput
                placeholder="Destino"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={destino}
                onChangeText={setDestino}
              />
            </View>

            <TouchableOpacity onPress={() => openDatePicker(true)}>
              <View style={styles.dateButton}>
                <Text style={styles.dateButtonText}>Selecionar Data de Ida</Text>
              </View>
            </TouchableOpacity>
            {ida !== '' && <Text style={styles.selectedDateText}>Data de Ida: {ida}</Text>}

            <TouchableOpacity onPress={() => openDatePicker(false)}>
              <View style={styles.dateButton}>
                <Text style={styles.dateButtonText}>Selecionar Data de Volta</Text>
              </View>
            </TouchableOpacity>
            {volta !== '' && <Text style={styles.selectedDateText}>Data de Volta: {volta}</Text>}

            <View style={styles.formAction}>
              <TouchableOpacity onPress={handleSearch}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Buscar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>

      {showDatePicker && (
        <DateTimePicker
          value={selectedDate}
          mode="date"
          display="default"
          onChange={handleSelectDate}
        />
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 24,
    paddingHorizontal: 0,
  },
  title: {
    fontSize: 28,
    fontWeight: '500',
    color: '#281b52',
    textAlign: 'center',
    marginBottom: 12,
    lineHeight: 40,
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 36,
  },
  form: {
    paddingHorizontal: 24,
    marginBottom: 24,
  },
  formAction: {
    marginTop: 4,
    marginBottom: 16,
  },
  input: {
    marginBottom: 16,
  },
  inputLabel: {
    fontSize: 17,
    fontWeight: '600',
    color: '#222',
    marginBottom: 8,
  },
  inputControl: {
    height: 50,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    borderRadius: 12,
    fontSize: 15,
    fontWeight: '500',
    color: '#222',
    borderWidth: 1,
    borderColor: '#C9D3DB',
    borderStyle: 'solid',
  },
  button: {
    backgroundColor: '#56409e',
    paddingVertical: 9,
    paddingHorizontal: 8,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 15,
    color: '#fff',
  },
  dateButton: {
    backgroundColor: '#56409e',
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 12,
    marginBottom: 10,
  },
  dateButtonText: {
    fontSize: 15,
    color: '#fff',
    textAlign: 'center',
  },
  selectedDateText: {
    fontSize: 15,
    color: '#281b52',
    textAlign: 'center',
    marginBottom: 10,
  },
});
